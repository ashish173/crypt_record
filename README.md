# CryptRecord

Here we go! saving you all from the hassle of keeping track of all your pending transactions.

UI Screengrab [link](https://imgur.com/a/zvxcQ)
There is some info's like date, to, from for a transaction etc which I have not displayed so far as I was mostly interested in status of the transaction.
This is something I was planning to work on but didn't get time.

### Configuring CryptRecord

1. Clone the Git repo from Bitbucket:

    ```console
    $ git clone https://bitbucket.org/ashish173/crypt_record.git
    ```

2. Install mix dependencies:

    ```console
    $ cd crypt_record
    $ mix deps.get
    ```

3. Create the event store database:

    ```console
    $ mix do event_store.create, event_store.init
    ```

4. Create the read model store database:

    ```console
    $ mix do ecto.create, ecto.migrate
    ```

5. Install React dependency
    ```console
    $ cd apps/web/assets

    # Next step is something I am still trying to figure out how to correctly do it.
    $ npm i react_phoenix@file:../../../deps/react_phoenix && npm i
    ```

6. Run the Phoenix server from root folder:

    ```console
    $ mix compile --recursive
    $ mix phx.server
    ```

  This will start the web server on localhost, port 4000: [http://0.0.0.0:4000](http://0.0.0.0:4000)

### Client to Server Flow

1. User enters transaction hash.

2. Transaction hash sent to server, using api call.

3. Transaction status is checked on etherum server.

4. Submit transaction events are fired.

5. After saving to DB, `create update` is broadcasted to the client using sockets.

6. User screen is updated with new transaction item in the transaction list.

7. If transaction status is not `success`, client sends update status ping periodically every 5sec.

8. And server returns the status through socket when udpate is complete.


### Design motivations

Have some **personal planning** material for design thinking here that would rather go on a white board. Not sure if it would really help but sharing just in case.

* Enter transaction hash planning [Imgur link](https://imgur.com/a/HIo9N)
* Fetch latest transaction status planning [Imgur link](https://imgur.com/a/yXVqq)

### Test cases

Added some test cases in `aggregates` app, however I would love to add more.
