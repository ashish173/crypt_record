defmodule CryptRecord.Transactions.CommandHandler do
  @behaviour Commanded.Commands.Handler

  alias CryptRecord.Commands.{SubmitTransactionHash, UpdateTransaction}
  alias CryptRecord.Aggregates.Transaction

  def handle(
        %Transaction{} = transaction,
        %SubmitTransactionHash{} = submit_transaction_hash
      ) do
    transaction
    |> Transaction.submit_transaction_hash(%SubmitTransactionHash{
      submit_transaction_hash
      | transaction_hash: submit_transaction_hash.transaction_hash,
        receipt_status: submit_transaction_hash.receipt_status,
        block_height: submit_transaction_hash.block_height,
        timestamp: submit_transaction_hash.timestamp,
        from: submit_transaction_hash.from,
        to: submit_transaction_hash.to,
        value: submit_transaction_hash.value,
        gas_limit: submit_transaction_hash.gas_limit,
        gas_used: submit_transaction_hash.gas_used,
        transaction_cost: submit_transaction_hash.transaction_cost,
        nonce: submit_transaction_hash.nonce
    })
  end

  def handle(
        %Transaction{} = transaction,
        %UpdateTransaction{} = update_transaction
      ) do
    transaction
    |> Transaction.update_transaction(%UpdateTransaction{
      update_transaction
      | transaction_hash: update_transaction.transaction_hash,
        receipt_status: update_transaction.receipt_status,
        block_height: update_transaction.block_height,
        timestamp: update_transaction.timestamp,
        from: update_transaction.from,
        to: update_transaction.to,
        value: update_transaction.value,
        gas_limit: update_transaction.gas_limit,
        gas_used: update_transaction.gas_used,
        transaction_cost: update_transaction.transaction_cost,
        nonce: update_transaction.nonce
    })
  end
end
