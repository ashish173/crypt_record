defmodule CryptRecord.Transactions.Router do
  use Commanded.Commands.Router

  alias CryptRecord.Transactions.CommandHandler
  alias CryptRecord.Aggregates.Transaction

  alias CryptRecord.Commands.{SubmitTransactionHash, UpdateTransaction}
  
  # middleware Commanded.Middleware.Auditing
  middleware(Commanded.Middleware.Logger)

  identify(Transaction, by: :transaction_hash, prefix: "transaction-")

  dispatch(
    [
      SubmitTransactionHash,
      UpdateTransaction
    ],
    to: CommandHandler,
    aggregate: Transaction,
    identity: :transaction_hash
  )
end
