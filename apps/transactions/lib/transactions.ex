defmodule CryptRecord.Transactions do
  @moduledoc """
  Context: Boundary for the Transactions
  """
  alias CryptRecord.Projections.Transaction

  @doc """
  Submit transaction hash
  """
  def submit_transaction(%Transaction{} = transaction, attrs \\ %{}) do
    # Fetch transaction details from the API.
    # Replace this dummy struct with API parsed resp.
    transaction = %Transaction{
      transaction_hash: "0x7b6d0e8d812873260291c3f8a9fa99a61721a033a01e5c5af3ceb5e1dc9e7bd0",
      receipt_status: "success",
      block_height: "4954885",
      timestamp: "Jan-22-2018 11:19:34 PM +UTC",
      from: "0x0fe426d8f95510f4f0bac19be5e1252c4127ee00",
      to: "0x4848535892c8008b912d99aaf88772745a11c809",
      value: "0.371237",
      gas_limit: "21000",
      gas_used: "21000",
      transaction_cost: "0.00042",
      nonce: "2"
    }

    # Router dispatch event.
    # with :ok <- Router.dispatch(favorite_article, consistency: :strong),
    #      {:ok, article} <- get(Article, article_uuid) do
    #   {:ok, %Article{article | favorited: true}}
    # else
    #   reply -> reply
    # end
  end
end
