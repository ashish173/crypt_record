defmodule CryptRecord.Events.TransactionHashSubmitted do
  defstruct [
    :transaction_hash,
    :receipt_status,
    :block_height,
    :timestamp,
    :from,
    :to,
    :value,
    :gas_limit,
    :gas_used,
    :transaction_cost,
    :nonce
  ]
end
