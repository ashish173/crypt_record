defmodule CryptRecord.Events do
  @moduledoc """
  Documentation for CryptRecord.Events.
  """

  @doc """
  Hello world.

  ## Examples

      iex> CryptRecord.Events.hello
      :world

  """
  def hello do
    :world
  end
end
