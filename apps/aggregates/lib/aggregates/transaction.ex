defmodule CryptRecord.Aggregates.Transaction do
  defstruct transaction_hash: nil,
            receipt_status: nil,
            block_height: nil,
            timestamp: nil,
            from: nil,
            to: nil,
            value: nil,
            gas_limit: nil,
            gas_used: nil,
            transaction_cost: nil,
            nonce: nil

  alias CryptRecord.Aggregates.Transaction
  alias CryptRecord.Commands.{SubmitTransactionHash, UpdateTransaction}

  alias CryptRecord.Events.{TransactionHashSubmitted, TransactionUpdated}

  # def submit_transaction_hash(
  #     %Transaction{},
  #     %SubmitTransactionHash{}
  # ) do
  #     {:error, :missing_transaction_hash}
  # end 

  def submit_transaction_hash(
        %Transaction{transaction_hash: transaction_hash},
        %SubmitTransactionHash{} = submit_transaction_hash
      ) do
    IO.inspect(["SubmitTransactionHash", submit_transaction_hash])

    %TransactionHashSubmitted{
      transaction_hash: submit_transaction_hash.transaction_hash,
      receipt_status: submit_transaction_hash.receipt_status,
      block_height: submit_transaction_hash.block_height,
      timestamp: submit_transaction_hash.timestamp,
      from: submit_transaction_hash.from,
      to: submit_transaction_hash.to,
      value: submit_transaction_hash.value,
      gas_limit: submit_transaction_hash.gas_limit,
      gas_used: submit_transaction_hash.gas_used,
      transaction_cost: submit_transaction_hash.transaction_cost,
      nonce: submit_transaction_hash.nonce
    }
  end

  def update_transaction(
        %Transaction{transaction_hash: transaction_hash},
        %UpdateTransaction{} = update_transaction
      ) do
    IO.inspect(["UpdateTransaction", update_transaction])

    %TransactionUpdated{
      transaction_hash: update_transaction.transaction_hash,
      receipt_status: update_transaction.receipt_status || "pending",
      block_height: update_transaction.block_height,
      timestamp: update_transaction.timestamp,
      from: update_transaction.from,
      to: update_transaction.to,
      value: update_transaction.value,
      gas_limit: update_transaction.gas_limit,
      gas_used: update_transaction.gas_used,
      transaction_cost: update_transaction.transaction_cost,
      nonce: update_transaction.nonce
    }
  end

  # state mutators
  def apply(%Transaction{} = transaction, %TransactionHashSubmitted{} = submitted) do
    IO.inspect(["TransactionHashSubmitted", transaction, submitted])

    %Transaction{
      transaction
      | transaction_hash: submitted.transaction_hash,
        receipt_status: submitted.receipt_status,
        block_height: submitted.block_height,
        timestamp: submitted.timestamp,
        from: submitted.from,
        to: submitted.to,
        value: submitted.value,
        gas_limit: submitted.gas_limit,
        gas_used: submitted.gas_used,
        transaction_cost: submitted.transaction_cost,
        nonce: submitted.nonce
    }
  end

  def apply(%Transaction{} = transaction, %TransactionUpdated{} = updated) do
    IO.inspect(["TransactionUpdated", transaction, updated])

    %Transaction{
      transaction
      | receipt_status: updated.receipt_status || "pending",
        block_height: updated.block_height,
        timestamp: updated.timestamp,
        from: updated.from,
        to: updated.to,
        value: updated.value,
        gas_limit: updated.gas_limit,
        gas_used: updated.gas_used,
        transaction_cost: updated.transaction_cost,
        nonce: updated.nonce
    }
  end
end
