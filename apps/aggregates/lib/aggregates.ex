defmodule CryptRecord.Aggregates do
  @moduledoc """
  Documentation for CryptRecord.Aggregates.
  """

  @doc """
  Hello world.

  ## Examples

      iex> CryptRecord.Aggregates.hello
      :world

  """
  def hello do
    :world
  end
end
