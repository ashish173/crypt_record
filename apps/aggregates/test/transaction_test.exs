defmodule CryptRecord.Factory do
  use ExMachina

  alias CryptRecord.Commands.{
    SubmitTransactionHash,
    UpdateTransaction
  }

  def transaction_factory do
    %{
      transaction_hash: "Test",
      reciept_status: "success"
    }
  end

  def submit_transaction_hash_factory do
    struct(SubmitTransactionHash, build(:transaction))
  end

  def update_transaction_factory do
    struct(UpdateTransaction, build(:transaction))
  end
end

defmodule CryptRecord.AggregateCase do
  @moduledoc """
  This module defines the test case to be used by aggregate tests.
  """

  use ExUnit.CaseTemplate

  using aggregate: aggregate, command_handler: command_handler do
    quote bind_quoted: [aggregate: aggregate, command_handler: command_handler] do
      @aggregate_module aggregate
      @command_handler command_handler

      import CryptRecord.Factory

      # assert that the expected events are returned when the given commands
      # have been executed
      defp assert_events(commands, expected_events) do
        assert_events(%@aggregate_module{}, commands, expected_events)
      end

      defp assert_events(aggregate, commands, expected_events) do
        {_aggregate, events, error} = execute(commands, aggregate)

        assert is_nil(error)
        assert List.wrap(events) == expected_events
      end

      defp assert_error(commands, expected_error) do
        assert_error(%@aggregate_module{}, commands, expected_error)
      end

      defp assert_error(aggregate, commands, expected_error) do
        {_aggregate, _events, error} = execute(commands, aggregate)

        assert error == expected_error
      end

      # execute one or more commands against an aggregate
      defp execute(commands, aggregate \\ %@aggregate_module{})

      defp execute(commands, aggregate) do
        commands
        |> List.wrap()
        |> Enum.reduce({aggregate, [], nil}, fn
          command, {aggregate, _events, nil} ->
            case @command_handler.handle(aggregate, command) do
              {:error, reason} = error -> {aggregate, nil, error}
              events -> {evolve(aggregate, events), events, nil}
            end

          command, {aggregate, _events, _error} = reply ->
            reply
        end)
      end

      # apply the given events to the aggregate state
      defp evolve(aggregate, events) do
        events
        |> List.wrap()
        |> Enum.reduce(aggregate, &@aggregate_module.apply(&2, &1))
      end
    end
  end
end

defmodule CryptRecord.TransactionTest do
  use CryptRecord.AggregateCase,
    aggregate: CryptRecord.Aggregates.Transaction,
    command_handler: CryptRecord.Transactions.CommandHandler

  alias CryptRecord.Commands.{
    SubmitTransactionHash,
    UpdateTransaction
  }

  alias CryptRecord.Events.{TransactionHashSubmitted, TransactionUpdated}

  describe "Submit Transaction" do
    @tag :unit
    test "should succeed" do
      transaction_hash = "Testing"

      assert_events(build(:submit_transaction_hash, transaction_hash: transaction_hash), [
        %TransactionHashSubmitted{
          transaction_hash: transaction_hash,
          receipt_status: "",
          block_height: "",
          timestamp: "",
          from: "",
          to: "",
          value: "",
          gas_limit: "",
          gas_used: "",
          transaction_cost: "",
          nonce: ""
        }
      ])
    end
  end

  describe "Update Transaction" do
    @tag :unit
    test "should update transaction" do
      transaction_hash = "Test"

      assert_events(build(:update_transaction, receipt_status: "pending"), [
        %TransactionUpdated{
          transaction_hash: transaction_hash,
          receipt_status: "pending",
          block_height: "",
          timestamp: "",
          from: "",
          to: "",
          value: "",
          gas_limit: "",
          gas_used: "",
          transaction_cost: "",
          nonce: ""
        }
      ])
    end
  end
end
