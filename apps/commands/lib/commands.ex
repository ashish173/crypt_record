defmodule CryptRecord.Commands do
  @moduledoc """
  Documentation for CryptRecord.Commands.
  """

  @doc """
  Hello world.

  ## Examples

      iex> CryptRecord.Commands.hello
      :world

  """
  def hello do
    :world
  end
end
