defmodule CryptRecord.Projectors do
  @moduledoc """
  Documentation for CryptRecord.Projectors.
  """

  @doc """
  Hello world.

  ## Examples

      iex> CryptRecord.Projectors.hello
      :world

  """
  def hello do
    :world
  end
end
