defmodule CryptRecord.Projectors.Transaction do
  use Commanded.Projections.Ecto,
    name: "CryptRecord.Projectors.Transaction",
    repo: CryptRecord.Projections.Repo,
    consistency: :strong

  alias CryptRecord.Events.{TransactionHashSubmitted, TransactionUpdated}
  alias CryptRecord.Projections.Transaction, as: TransactionProjection
  alias CryptRecord.Projections.Repo

  alias CryptRecord.WebWeb.TransactionsChannel

  project %TransactionHashSubmitted{transaction_hash: ""} = submitted, _, do: multi
  project %TransactionHashSubmitted{} = submitted, _ do
    IO.inspect(["Transaction Submitted Projector", submitted])

    transaction = submitted.transaction_hash
    |> find_or_create_transaction
    |> TransactionProjection.changeset(%{
      receipt_status: submitted.receipt_status,
      block_height: submitted.block_height,
      timestamp: submitted.timestamp,
      from: submitted.from,
      to: submitted.to,
      value: submitted.value,
      gas_limit: submitted.gas_limit,
      gas_used: submitted.gas_used,
      transaction_cost: submitted.transaction_cost,
      nonce: submitted.nonce
    })
    |> Repo.insert_or_update!()

    TransactionsChannel.broadcast_create(transaction)

    multi
  end

  project %TransactionUpdated{transaction_hash: ""} = updated, _, do: multi
  project %TransactionUpdated{} = updated, _ do
    IO.inspect(["Transaction Updated Projector", updated])

    transaction = updated.transaction_hash
    |> find_or_create_transaction
    |> TransactionProjection.changeset(%{
      transaction_hash: updated.transaction_hash,
      receipt_status: updated.receipt_status || "pending",
      block_height: updated.block_height,
      timestamp: updated.timestamp,
      from: updated.from,
      to: updated.to,
      value: updated.value,
      gas_limit: updated.gas_limit,
      gas_used: updated.gas_used,
      transaction_cost: updated.transaction_cost,
      nonce: updated.nonce
    })
    |> Repo.insert_or_update!()

    TransactionsChannel.broadcast_create(transaction)

    multi
  end

  def find_or_create_transaction(transaction_hash) do
    case Repo.get_by(TransactionProjection, transaction_hash: transaction_hash) do
      nil -> %TransactionProjection{transaction_hash: transaction_hash}
      post -> post
    end
  end
end
