import React, { Component } from "react";
import { Route } from "react-router-dom";
import axios from 'axios';

class TransactionListItem extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const updateInterval = setInterval(() => {
      const { transaction_hash, receipt_status } = this.props.transaction;

      if (['success', 'failed'].indexOf(receipt_status) !== -1) {
        clearInterval(updateInterval);
        return;
      }

      axios.get('/transaction/status', {
        params: {
          transaction_hash: transaction_hash
        }
      })
        .then(function (response) {
          console.log("Updating transaction for : ", transaction_hash, ", Reponse : ", response.data);
        })
        .catch(function (error) {
          console.log("Update error for : ", transaction_hash, " is : ", error);
        });
    }, 5000);
  }

  render() {
    const {
      transaction_hash, receipt_status,
      block_height, timestamp, from, to,
      value, gas_limit, gas_used, nonce,
      transaction_cost
    } = this.props.transaction;
    return (
      <div className="transaction-list-item-card">
        <h3 className="transaction-list-item-header">
          <small>Transaction hash : </small>
          <strong>{transaction_hash}</strong>
          <strong className={`transaction-list-item-receipt-status ${receipt_status}`}>{receipt_status}</strong>
        </h3>
        <legend>
          <strong>Date :</strong> {timestamp} |
          <strong> From :</strong> {from} | <strong>To :</strong> {to}
        </legend>
        <p>
          <strong> Block Height :</strong> {block_height} |
          <strong> Value :</strong> {value} |
          <strong> Gas Limit :</strong> {gas_limit} |
          <strong> Gas Used :</strong> {gas_used} |
          <strong> Transaction Cost :</strong> {transaction_cost} |
          <strong> Nonce :</strong> {nonce}
        </p>
      </div>
    )
  }
}

export default TransactionListItem;
