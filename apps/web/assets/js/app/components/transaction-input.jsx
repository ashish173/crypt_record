import React, { Component } from "react";
import { Route } from "react-router-dom";

class TransactionInput extends Component {
  constructor(props) {
    super(props);
    this.state = { inputHash: '' };
  }

  onSubmitted() {
    this.props.onTransactionHashSubmit(this.state.inputHash);
    this.setState({inputHash: ''});
  }

  render() {
    return (
      <div className="transaction-input-container">
        <input
          value={this.state.inputHash}
          type="text" className="transaction-input"
          placeholder="Your transaction hash here"
          onChange={event => this.setState({ inputHash: event.target.value })}
        />

        <input
          type="button"
          className="transaction-input-submit"
          value="Submit"
          onClick={this.onSubmitted.bind(this)}
        />
      </div>
    )
  }
}

export default TransactionInput;
