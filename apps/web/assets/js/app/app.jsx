import React, { Component } from "react";
import Dashboard from "./containers/dashboard";

class App extends Component {
  render() {
    return (
      <div className="main">
        <Dashboard transactions={this.props.transactions} />
      </div>
    )
  }
}

export default App;
