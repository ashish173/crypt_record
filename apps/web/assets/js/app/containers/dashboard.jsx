import React, { Component } from "react";
import { Route } from "react-router-dom";

import TransactionInput from '../components/transaction-input';
import TransactionListItem from "../components/transaction-list-item";
import { channel } from './../../socket';

import axios from 'axios';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = { transactions: [] };
  }

  componentDidMount() {
    this.setState({ transactions: this.props.transactions });

    channel.on("new:transaction", newTransaction => {
      const transactions = this.state.transactions.filter(data => data.transaction_hash !== newTransaction.transaction_hash);
      this.setState({
        transactions: [newTransaction, ...transactions]
      });
    })

    channel.on("update:transaction", updatedTransaction => {
      const transactions = this.state.transactions.map(data => {
        if (data.transaction_hash === updatedTransaction.transaction_hash) { data = updatedTransaction }
        return data
      });
      this.setState({
        transactions: [...transactions]
      });
    })
  }

  transactionHashSubmitted(inputHash) {
    axios.post('/transaction/submit', {
      transaction_hash: inputHash
    })
    .then(function (response) {
      console.log("Submitting transaction hash : ", inputHash, ", Response : ", response.data);
    })
    .catch(function (error) {
      console.log("Submit error for : ", inputHash, " is : ", error);      
    });
  }

  renderTransactionList() {
    if (this.state.transactions.length) {
      return (
        this.state.transactions.map(
          transaction => <TransactionListItem key={transaction.transaction_hash} transaction={transaction} />
        )
      )
    } else {
      return <h3 className="transaction-list-empty-header">No transaction added</h3>
    }
  }

  render() {
    return (
      <div className="dashboard">
        <h3 className="dashboard-header">UTRUST PAYMENTS</h3>
        <TransactionInput onTransactionHashSubmit={this.transactionHashSubmitted.bind(this)} />
        <div className="transaction-list-container">
          {this.renderTransactionList()}
        </div>
      </div>
    )
  }
}

export default Dashboard;
