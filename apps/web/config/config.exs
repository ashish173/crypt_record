# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :web,
  namespace: CryptRecord.Web,
  ecto_repos: [CryptRecord.Web.Repo]

# Configures the endpoint
config :web, CryptRecord.WebWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "b+yK/Sd1eBA40M8nUVH6IiuV7VZCK8bVC6pOkXvScEJG8+GDKaCFiaOYyUgf2INC",
  render_errors: [view: CryptRecord.WebWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: CryptRecord.Web.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
