defmodule CryptRecord.Dashboard do
  alias CryptRecord.Transactions.Router
  alias CryptRecord.Transactions.CommandHandler
  alias CryptRecord.Projections.Transaction
  alias CryptRecord.Commands.{SubmitTransactionHash, UpdateTransaction}
  alias CryptRecord.Projections.Repo
  alias CryptRecord.WebWeb.TransactionView

  import Ecto.Query

  def get_all_transactions do
    Repo.all(from t in Transaction, order_by: [desc: t.inserted_at])
  end

  def get_all_transactions_json do
    TransactionView.render("index.json", %{transactions: get_all_transactions}).data
  end

  def submit_transaction_hash(transaction_hash) do
    with {:ok, status} <- check_transaction_status(transaction_hash) do
      %SubmitTransactionHash{transaction_hash: transaction_hash, receipt_status: status}
      |> router_dispatcher
    else
      {:error, reason} -> reason
    end
  end

  def update_transaction(transaction_hash) do
    with {:ok, status} <- check_transaction_status(transaction_hash) do
      %UpdateTransaction{transaction_hash: transaction_hash, receipt_status: status}
      |> router_dispatcher
    else
      {:error, reason} -> reason
    end
  end

  defp check_transaction_status(transaction_hash, retry_count \\ 3) do
    url =
      "https://etherscan.io/tx/#{transaction_hash}"

    with {
           :ok,
           %HTTPoison.Response{body: body}
         } <- HTTPoison.post(url, "") do
      try do
        with [{"span", [_], [receipt_status]}] <- Floki.find(body, "span[title='No of Blocks Mined Since']"),
             true <-  (receipt_status |> String.split |> List.first |> String.to_integer) >= 2
        do
          IO.inspect receipt_status
          {:ok, "success"}
        else
          _ -> {:ok, "pending"}
        end
      rescue
        _ -> {:error, "failed parsing"}
      end
    else
      {:error, %HTTPoison.Error{reason: reason}} ->
        if retry_count == 3 do
          {:error, reason}
        else
          check_transaction_status(transaction_hash, retry_count + 1)
        end
    end
  end

  defp get_transaction(transaction_hash) do
    case Repo.get_by(Transaction, transaction_hash: transaction_hash) do
      nil -> {:error, :not_found}
      projection -> {:ok, projection}
    end
  end

  defp router_dispatcher(command) do
    with :ok <- Router.dispatch(command) do
      get_transaction(command.transaction_hash)
    else
      reply -> reply
    end
  end
end
