defmodule CryptRecord.WebWeb.TransactionView do
  use CryptRecord.WebWeb, :view
  alias CryptRecord.WebWeb.TransactionView

  def render("index.json", %{transactions: transactions}) do
    %{data: render_many(transactions, TransactionView, "transaction.json")}
  end

  def render("show.json", %{transaction: transaction}) do
    %{data: render_one(transaction, TransactionView, "transaction.json")}
  end

  def render("transaction.json", %{transaction: transaction}) do
    %{
      transaction_hash: transaction.transaction_hash,
      receipt_status: transaction.receipt_status,
      block_height: transaction.block_height,
      timestamp: transaction.timestamp,
      from: transaction.from,
      to: transaction.to,
      value: transaction.value,
      gas_limit: transaction.gas_limit,
      gas_used: transaction.gas_used,
      transaction_cost: transaction.transaction_cost,
      nonce: transaction.nonce
    }
  end
end
