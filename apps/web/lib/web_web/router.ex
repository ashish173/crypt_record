defmodule CryptRecord.WebWeb.Router do
  use CryptRecord.WebWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    # plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", CryptRecord.WebWeb do
    # Use the default browser stack
    pipe_through(:browser)

    get("/", DashboardController, :index)
    post("/transaction/submit", DashboardController, :submit_transaction)
    get("/transaction/status", DashboardController, :check_transaction_status)
  end

  # Other scopes may use custom stacks.
  # scope "/api", CryptRecord.WebWeb do
  #   pipe_through :api
  # end
end
