defmodule CryptRecord.WebWeb.TransactionsChannel do
  use CryptRecord.WebWeb, :channel
  alias CryptRecord.WebWeb.Endpoint

  def join("transactions:lobby", payload, socket) do
    if authorized?(payload) do
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (transactions:lobby).
  def handle_in("shout", payload, socket) do
    broadcast(socket, "shout", payload)
    {:noreply, socket}
  end

  def broadcast_create(transaction) do
    payload =
      CryptRecord.WebWeb.TransactionView.render("transaction.json", %{transaction: transaction})

    Endpoint.broadcast("transactions:lobby", "new:transaction", payload)
  end

  def broadcast_update(transaction) do
    payload =
      CryptRecord.WebWeb.TransactionView.render("transaction.json", %{transaction: transaction})

    Endpoint.broadcast("transactions:lobby", "update:transaction", payload)
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
end
