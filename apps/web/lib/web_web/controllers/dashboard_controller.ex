defmodule CryptRecord.WebWeb.DashboardController do
  use CryptRecord.WebWeb, :controller

  alias CryptRecord.Projections.Transaction
  alias CryptRecord.Dashboard

  def index(conn, _params) do
    transactions = Dashboard.get_all_transactions_json
    render(conn, "index.html", transactions: transactions)
  end

  def submit_transaction(conn, %{"transaction_hash" => transaction_hash}) do
    with {:ok, %Transaction{} = transaction} <- Dashboard.submit_transaction_hash(transaction_hash) do
      text conn, "Submitted transaction hash => #{transaction_hash}"
    else
      err ->
        IO.inspect err
        text conn, "Submittion failed for transaction hash => #{transaction_hash}"
    end
  end

  def check_transaction_status(conn, %{"transaction_hash" => transaction_hash}) do
    with {:ok, %Transaction{} = transaction} <- Dashboard.update_transaction(transaction_hash) do
      text conn, "Updated transaction with hash => #{transaction_hash}"
    else
      err ->
        IO.inspect err
        text conn, "Updation failed for transaction hash => #{transaction_hash}"
    end
  end
end
