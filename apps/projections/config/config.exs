use Mix.Config

config :projections, ecto_repos: [CryptRecord.Projections.Repo]

import_config "#{Mix.env()}.exs"
