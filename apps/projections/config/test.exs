use Mix.Config

# Configure your database
config :projections, CryptRecord.Projections.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "projections_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
