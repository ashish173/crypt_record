defmodule CryptRecord.Projections.Transaction do
  use Ecto.Schema
  import Ecto.Changeset

  schema "transactions" do
    field(:transaction_hash, :string)
    field(:receipt_status, :string)
    field(:block_height, :string)
    field(:timestamp, :string)
    field(:from, :string)
    field(:to, :string)
    field(:value, :string)
    field(:gas_limit, :string)
    field(:gas_used, :string)
    field(:transaction_cost, :string)
    field(:nonce, :string)

    timestamps()
  end

  @required_fields ~w(transaction_hash)a
  @optional_fields ~w(receipt_status block_height block_height timestamp from to value gas_limit gas_used transaction_cost nonce)a

  def changeset(model, params \\ %{}) do
    model
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
  end
end
