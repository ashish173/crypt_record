defmodule CryptRecord.Projections.Application do
  @moduledoc """
  The CryptRecord.Projections Application Service.

  The projections system business domain lives in this application.

  Exposes API to clients such as the `CryptRecord.ProjectionsWeb` application
  for use in channels, controllers, and elsewhere.
  """
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    Supervisor.start_link(
      [
        supervisor(CryptRecord.Projections.Repo, [])
      ],
      strategy: :one_for_one,
      name: CryptRecord.Projections.Supervisor
    )
  end
end
