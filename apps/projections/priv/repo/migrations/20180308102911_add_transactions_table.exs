defmodule CryptRecord.Projections.Repo.Migrations.AddTransactionsTable do
  use Ecto.Migration

  def change do
    create table(:transactions) do
      add :transaction_hash, :string
      add :receipt_status, :string
      add :block_height, :string
      add :timestamp, :string
      add :from, :string
      add :to, :string
      add :value, :string
      add :gas_limit, :string
      add :gas_used, :string
      add :transaction_cost, :string
      add :nonce, :string

      timestamps()
    end

    create unique_index(:transactions, [:transaction_hash])
  end
end
